package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class SettingsActivity extends PreferenceActivity {

    //protected ClientBluetoothService clientBluetoothService;
    //protected boolean mBound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Intent intent = new Intent(this, ClientBluetoothService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //if (mBound) {
        //    unbindService(mConnection);
        //    mBound = false;
        //}
    }

    public void disconnect() throws IOException {
        if(((MyECG) this.getApplication()).getSocket() != null)
            ((MyECG) this.getApplication()).getSocket().close();
        //clientBluetoothService.disconnect();
    }

    public void unmount_sd() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        if(!prefs.getBoolean("connect_device", false)) {
            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
            return;
        }
        String input = null;
        try {
            BluetoothSocket socket = ((MyECG) this.getApplication()).getSocket();
            if(socket == null) {

                //Log.d("HERE", "NULL socket");
                return;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("UNMOUNT_SD");
            input = in.readLine();
            Log.d("HERE", "Input: " + input);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "SD card unmounted properly", Toast.LENGTH_LONG).show();
    }

    /*
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            ClientBluetoothService.LocalBinder binder = (ClientBluetoothService.LocalBinder) service;
            clientBluetoothService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    */

}

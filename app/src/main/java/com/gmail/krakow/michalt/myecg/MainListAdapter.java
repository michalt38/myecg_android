package com.gmail.krakow.michalt.myecg;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Michał on 06.10.2017.
 */

public class MainListAdapter extends BaseAdapter {

    private Context mContext;
    private List<MainListIteam> mMainList;

    public MainListAdapter(Context context, List<MainListIteam> mainList) {
        this.mContext = context;
        this.mMainList = mainList;
    }

    @Override
    public int getCount() {
        return mMainList.size();
    }

    @Override
    public Object getItem(int i) {
        return mMainList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.main_list, null);
        TextView text = (TextView)v.findViewById(R.id.text);
        ImageView icon = (ImageView) v.findViewById(R.id.icon);

        text.setText(mMainList.get(i).getText());
        icon.setImageResource(mMainList.get(i).getImage());

        v.setTag(mMainList.get(i).getId());
        return v;
    }
}

package com.gmail.krakow.michalt.myecg;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import dmax.dialog.SpotsDialog;

public class ShowDatabaseActivity extends AppCompatActivity {

    private ListView lv;
    String json_string;
    String json_string_sig;
    JSONObject jsonObject;
    JSONArray jsonArray;
    SignalDateAdapter signalDateAdapter;
    List<SignalDate> signalDateList;
    List<Integer> signal_values;
    List<Float> signal_times;
    SpotsDialog loadingDialog;

    @Override
    protected void onStop() {
        super.onStop();
        signal_values.clear();
        signal_times.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_database);
        signalDateList = new ArrayList<>();
        lv = (ListView)findViewById(R.id.datesListView);
        setTitle("Select signal");
        loadingDialog = new SpotsDialog(ShowDatabaseActivity.this, R.style.Loading);
        getJSON(String.valueOf(((MyECG) this.getApplication()).getUserId()));
        signal_values = new ArrayList<>();
        signal_times = new ArrayList<>();
    }

    public void getJSON(String s) {
        new GetDatesTask(s).execute();
    }

    class GetDatesTask extends AsyncTask<Void, Object, String>
    {
        String id;
        String JSON_String;
        GetDatesTask(String id) {
            this.id = id;
        }

        String json_url;
        @Override
        protected void onPreExecute() {
            loadingDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            json_url = "http://student.agh.edu.pl/~milchalt/myECG/get_dates.php?id=" + id;

            try {
                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                while((JSON_String = bufferedReader.readLine()) != null) {
                    stringBuilder.append(JSON_String + "\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            //super.onPostExecute(result);
            json_string = result;
            parseJSON_Dates();
        }
    }

    public void parseJSON_Dates() {
        if(json_string == null) {
            Toast.makeText(getApplicationContext(), "Empty database", Toast.LENGTH_SHORT).show();
        }

        try {
            jsonArray = new JSONArray(json_string);
            int count = 0;
            String id, date;
            while(count < jsonArray.length()) {
                JSONObject JO = jsonArray.getJSONObject(count);
                id = JO.getString("id");
                date = JO.getString("date");
                SignalDate signalDate = new SignalDate(id, date);
                signalDateList.add(count, signalDate);
                count++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        signalDateAdapter = new SignalDateAdapter(getApplicationContext(), signalDateList);
        lv.setAdapter(signalDateAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                new GetValuesTask(String.valueOf(signalDateList.get(i).getId())).execute();
            }
        });
        loadingDialog.dismiss();
    }

    class GetValuesTask extends AsyncTask<Void, Object, String> {
        String id;
        String JSON_String;
        GetValuesTask(String id) {
            this.id = id;
        }

        String json_url;
        @Override
        protected void onPreExecute() {
            loadingDialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            json_url = "http://student.agh.edu.pl/~milchalt/myECG/get_signal.php?signal=" + id;
            //Log.d("HERE", json_url);

            try {
                URL url = new URL(json_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder stringBuilder = new StringBuilder();
                while((JSON_String = bufferedReader.readLine()) != null) {
                    stringBuilder.append(JSON_String + "\n");
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return stringBuilder.toString().trim();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Object... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            //super.onPostExecute(result);
            json_string_sig = result;
            if(json_string_sig.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Empty signal", Toast.LENGTH_SHORT).show();
                return;
            }
            parseJSON_Signal();
        }
    }

    public void parseJSON_Signal() {
        if(json_string_sig == null) {
            Toast.makeText(getApplicationContext(), "Empty signal", Toast.LENGTH_SHORT).show();
        }

        try {
            //Log.d("HERE", json_string_sig);
            jsonArray = new JSONArray(json_string_sig);
            int count = 0;
            String value;
            String time;
            float ftime = 0;
            while(count < jsonArray.length()) {
                JSONObject JO = jsonArray.getJSONObject(count);
                value = JO.getString("value");
                time = JO.getString("time");
                ftime += Integer.valueOf(time)/1000;
                signal_values.add(Integer.valueOf(value));
                signal_times.add(ftime);
                count++;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        loadingDialog.dismiss();

        Intent intent = new Intent(ShowDatabaseActivity.this, DrawGraphActivity.class);
        intent.putIntegerArrayListExtra("data_values", (ArrayList<Integer>) signal_values);
        intent.putExtra("data_times", (Serializable) signal_times);
        //intent.putIntegerArrayListExtra("data_times", (ArrayList<Integer>) signal_times);
        startActivity(intent);
    }
}

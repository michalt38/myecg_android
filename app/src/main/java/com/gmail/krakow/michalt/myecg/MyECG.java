package com.gmail.krakow.michalt.myecg;

import android.app.Application;
import android.bluetooth.BluetoothSocket;

/**
 * Created by Michał on 03.11.2017.
 */

public class MyECG extends Application {

    private BluetoothSocket mmSocket;
    private String userName;
    private String userSurname;
    private int userId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String name) {
        userName = name;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String surname) {
        userSurname = surname;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int id) {
        userId = id;
    }

    public BluetoothSocket getSocket() {
        return mmSocket;
    }

    public void setSocket(BluetoothSocket socket) {
        mmSocket = socket;
    }
}

package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.UUID;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.HELLO;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.NULL;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

/**
 * Created by Michał on 01.09.2017.
 */

public class ClientBluetooth extends Thread implements Serializable{

    private String status;
    private boolean connected;
    private String command_to_send;
    private CommandDecoder commandDecoder;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private boolean msgDecoded;
    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;
    private String lastCommand;

    public ClientBluetooth() {
        connected = false;
        command_to_send = "";
        commandDecoder = new CommandDecoder();
        msgDecoded = false;
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
    }

    public BluetoothSocket getSocket() {
        return mmSocket;
    }

    public void setDevice(BluetoothDevice device) {
        BluetoothSocket tmp = null;
        mmDevice = device;
        try {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            tmp = device.createRfcommSocketToServiceRecord(uuid);
        } catch (Exception e) {}
        mmSocket = tmp;
    }

    public String getStatus(){
        return status;
    }

    public void run() {
        try {
            Log.d("INFO", "Próba połączenia....");
            status = "Connecting";
            mmSocket.connect();
            status = "Connected";
            Log.d("INFO", "Połączono z serwerem!");
            connected = true;
            BufferedReader in = new BufferedReader(new InputStreamReader(mmSocket.getInputStream()));
            String input = in.readLine();
            Log.d("INFO", "Serwer mówi: " + input);
            PrintWriter out = new PrintWriter(mmSocket.getOutputStream(), true);
            //out.println("HELLO");
            //input = in.readLine();
            //Log.d("INFO", "Serwer mówi: " + input);

            TokenValue token = new TokenValue();
            TokenValue token2 = new TokenValue();
            StringValue stringToken = new StringValue();
            StringValue stringToken2 = new StringValue();

            while (connected) {
                commandDecoder = new CommandDecoder();

                if(!command_to_send.isEmpty()) {
                    lastCommand = command_to_send;
                    Log.d("INFO", "SENDING COMMAND: " + command_to_send);
                    out.println(command_to_send);
                    Log.d("INFO", "COMMAND SENT");
                    input = in.readLine();
                    Log.d("INFO", "RESPONSE READ");
                    for(int i = 0; i < input.length(); i++) {
                        commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
                    }
                    commandDecoder.Receiver_PutCharacterToBuffer((char)10);
                    if(CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
                        StringValue received_msg = new StringValue();
                        //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
                        commandDecoder.Receiver_GetStringCopy(received_msg);
                        commandDecoder.DecodeMsg(received_msg);

                        if(_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                            Log.d("HERE", "MsgDecoded");
                            msgDecoded = true;
                            firstToken.set(token.get());

                            if(_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                                secondToken.set(token2.get());
                            }

                            if(_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                                msgString1.set(stringToken.get());
                            }

                            if(_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                                msgString2.set(stringToken2.get());
                            }

                            if (token.get() == HELLO)
                            {
                                    Log.d("INFO", "Odkodowano: HELLO!");
                            }
                        }
                    }
                    command_to_send = "";
                }
            }
        } catch (Exception ce) {
            try {
                disconnect();
            } catch (Exception cle) {}
        }

    }

    public void clearTokens() {
        firstToken.set(NULL);
        secondToken.set(NULL);
    }


    public void send_command(String command) {
        command_to_send = command;
    }

    public void disconnect() throws IOException {
        status = "Disconnected";
        connected = false;
        if(mmSocket != null)
            mmSocket.close();
        Log.d("HERE", "GOT EXCEPTION: DISCONNECTED!");
    }

    public boolean isMsgDecoded() {
        boolean d = msgDecoded;
        msgDecoded = false;
        return d;
    }

    public void getFirstToken(TokenValue token) {
        token.set(firstToken.get());
        firstToken.set(NULL);
    }

    public void getSecondToken(TokenValue token) {
        token.set(secondToken.get());
        secondToken.set(NULL);
    }

    public void getFirstStringToken(StringValue stingValue) {
        stingValue.set(msgString1.get());
    }

    public void getSecondStringToken(StringValue stingValue) {
        stingValue.set(msgString2.get());
    }

}

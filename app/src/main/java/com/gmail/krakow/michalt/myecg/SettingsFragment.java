package com.gmail.krakow.michalt.myecg;


import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends PreferenceFragment {

    final int CONNECT_DEVICE = 1;
    final int DISCONNECT_DEVICE = 2;
    final int LOG_IN = 3;
    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);

        final CheckBoxPreference device_connection = (CheckBoxPreference) findPreference("connect_device");
        final CheckBoxPreference log_in = (CheckBoxPreference) findPreference("log_in");
        final Preference unmount_sd = (Preference) findPreference("unmount_sd");

        device_connection.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(device_connection.isChecked()) {
                    Intent intent = new Intent(getActivity(), ConnectDeviceActivity.class);
                    startActivityForResult(intent, CONNECT_DEVICE);
                } else {
                    try {
                        ((SettingsActivity)getActivity()).disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                return true;
            }
        });

        log_in.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Log.d("HERE", "LOG IN CLICKED");
                if(log_in.isChecked()) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivityForResult(intent, LOG_IN);
                }

                return true;
            }
        });

        unmount_sd.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                ((SettingsActivity)getActivity()).unmount_sd();
                return true;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final CheckBoxPreference device_connection = (CheckBoxPreference) findPreference("connect_device");
        final CheckBoxPreference log_in = (CheckBoxPreference) findPreference("log_in");
        switch (requestCode){
            case CONNECT_DEVICE:
                if(resultCode == RESULT_OK)
                    device_connection.setChecked(true);
                else
                    device_connection.setChecked(false);
                break;
            case DISCONNECT_DEVICE:
                if(resultCode == RESULT_OK)
                    device_connection.setChecked(false);
                else
                    device_connection.setChecked(true);
                break;
            case LOG_IN:
                if(resultCode == RESULT_OK) {
                    log_in.setChecked(true);
                    String user = data.getStringExtra("USER");
                    log_in.setSummaryOn("Logged in as: " + user);
                }
                else
                    log_in.setChecked(false);
        }
    }
}

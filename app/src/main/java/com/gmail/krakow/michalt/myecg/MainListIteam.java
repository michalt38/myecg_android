package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 06.10.2017.
 */

public class MainListIteam {
    private int id;
    private String text;
    private int image;

    public MainListIteam(int id, String text, int image) {
        this.text = text;
        this.image = image;
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

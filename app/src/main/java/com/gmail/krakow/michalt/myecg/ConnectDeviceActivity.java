package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class ConnectDeviceActivity extends AppCompatActivity implements Serializable {

    //private ClientBluetooth clientBT;
    private ListView availableDevicesListView;
    private AvailableDevicesAdapter availableDevicesAdapter;
    private List<Device> devices;
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    String status = null;

    //protected boolean mBound = false;
    //private ClientBluetoothService clientBluetoothService;

    @Override
    protected void onStart() {
        super.onStart();
        //Intent intent = new Intent(this, ClientBluetoothService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //if (mBound) {
        //    unbindService(mConnection);
        //    mBound = false;
        //}
    }

    public void disconnect() throws IOException {
        //clientBluetoothService.disconnect();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_connect_device);
        setTitle("Choose device");

        devices = new ArrayList<>();
        //BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(mBluetoothAdapter == null) {
            Toast.makeText(ConnectDeviceActivity.this, "This device does not have Bluetooth capabilities", Toast.LENGTH_LONG).show();
            return;
        }else if(!mBluetoothAdapter.isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent, 1);
        }else
            fillListView();
        /*
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0) {
            int i = 0;
            for(BluetoothDevice device : pairedDevices) {
                //Log.d("INFO", device.getName() + " - " + device.getAddress());
                devices.add(new Device(i++, device.getName(), device.getAddress()));
            }
        }

        availableDevicesAdapter = new AvailableDevicesAdapter(getApplicationContext(), devices);
        devicesListView();
        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 1:
                fillListView();
                break;
        }
    }

    private void fillListView() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if(pairedDevices.size() > 0) {
            int i = 0;
            for(BluetoothDevice device : pairedDevices) {
                //Log.d("INFO", device.getName() + " - " + device.getAddress());
                devices.add(new Device(i++, device.getName(), device.getAddress()));
            }
        }

        availableDevicesAdapter = new AvailableDevicesAdapter(getApplicationContext(), devices);
        devicesListView();
    }

    public void devicesListView() {
        availableDevicesListView = (ListView) findViewById(R.id.availableDevicesListView);
        availableDevicesListView.setAdapter(availableDevicesAdapter);
        availableDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(ConnectDeviceActivity.this, devices.get(i).getName(), Toast.LENGTH_LONG).show();
                BluetoothDevice server = mBluetoothAdapter.getRemoteDevice(devices.get(i).getAddress());
                //clientBT = new ClientBluetooth();
                //clientBT.setDevice(server);
                //clientBT.start();
                //clientBluetoothService.newClient();
                BluetoothSocket tmp = null;
                //mmDevice = device;
                setDevice(server);
                //clientBluetoothService.setDevice(server);
                //checkConnection();

                SpotsDialog dialog = new SpotsDialog(ConnectDeviceActivity.this, R.style.Connecting);
                dialog.show();
                WaitForConnect waitForConnect = new WaitForConnect(dialog);
                waitForConnect.start();

            }
        });
    }

    private void setDevice(BluetoothDevice device) {
        BluetoothSocket tmp = null;
        BluetoothDevice mmDevice = device;
        try {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            tmp = device.createRfcommSocketToServiceRecord(uuid);
        } catch (Exception e) {}
        ((MyECG) this.getApplication()).setSocket(tmp);
    }

    private void checkConnection() {
        try {
            Log.d("INFO", "Próba połączenia....");
            status = "Connecting";
            ((MyECG) this.getApplication()).getSocket().connect();
            status = "Connected";
            Log.d("INFO", "Połączono z serwerem!");
            BufferedReader in = new BufferedReader(new InputStreamReader(((MyECG) this.getApplication()).getSocket().getInputStream()));
            String input = in.readLine();
            Log.d("INFO", "Serwer mówi: " + input);
        } catch (IOException e) {
            status = "Disconnected";
        }
    }

    private class WaitForConnect extends Thread {

        private SpotsDialog dialog;
        //private ClientBluetooth clientBT;

        public WaitForConnect(SpotsDialog dialog) {
            this.dialog = dialog;
            //this.clientBT = clientBT;
        }

        public void run() {
            checkConnection();
            while(true) {
                //dialog.show();
                if(status == "Connected") {
                    dialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ConnectDeviceActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                        }
                    });
                    Intent intent = new Intent();
                    //intent.putExtra("clientBT", clientBT);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                }
                if(status == "Disconnected") {
                    dialog.dismiss();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ConnectDeviceActivity.this, "Disconnected", Toast.LENGTH_SHORT).show();
                        }
                    });
                    break;
                }
            }
        }
    }

    /*
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {

            ClientBluetoothService.LocalBinder binder = (ClientBluetoothService.LocalBinder) service;
            clientBluetoothService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    */
}

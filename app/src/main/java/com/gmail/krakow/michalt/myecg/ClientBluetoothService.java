package com.gmail.krakow.michalt.myecg;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.UUID;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.NULL;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

public class ClientBluetoothService extends Service {

    private String status;
    private boolean connected;
    private String command_to_send;
    private CommandDecoder commandDecoder;
    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private boolean msgDecoded;
    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;

    private Handler handler = new Handler();

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        ClientBluetoothService getService() {
            return ClientBluetoothService.this;
        }
    }

    public ClientBluetoothService() {
        connected = false;
        command_to_send = "";
        commandDecoder = new CommandDecoder();
        msgDecoded = false;
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void generateToast() {
        Log.d("HERE", "Service: generateToast()");
        Toast.makeText(getApplicationContext(), "WITAJ W SERWISIE", Toast.LENGTH_SHORT).show();
    }

    public void setDevice(BluetoothDevice device) {
        BluetoothSocket tmp = null;
        mmDevice = device;
        try {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            tmp = device.createRfcommSocketToServiceRecord(uuid);
        } catch (Exception e) {}
        mmSocket = tmp;
    }

    public void sendCommand(String command) {
        commandDecoder = new CommandDecoder();
        String input = null;
        try {
            if (mmSocket == null) {

                //Log.d("HERE", "NULL socket");
                return;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(mmSocket.getInputStream()));
            PrintWriter out = new PrintWriter(mmSocket.getOutputStream(), true);
            out.println(command);
            input = in.readLine();
            Log.d("HERE", "Input: " + input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TokenValue token = new TokenValue();
        TokenValue token2 = new TokenValue();
        StringValue stringToken = new StringValue();
        StringValue stringToken2 = new StringValue();

        for (int i = 0; i < input.length(); i++) {
            commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
        }
        commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
        if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
            StringValue received_msg = new StringValue();
            //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
            commandDecoder.Receiver_GetStringCopy(received_msg);
            commandDecoder.DecodeMsg(received_msg);

            if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                firstToken.set(token.get());

                if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                    secondToken.set(token2.get());
                }

                if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                    msgString1.set(stringToken.get());
                }

                if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                    msgString2.set(stringToken2.get());
                }
            }
        }
    }

    //public void newClient() {
    //    clientBT = new ClientBluetooth();
    //}

    public void start() {
        try {
            Log.d("INFO", "Próba połączenia....");
            status = "Connecting";
            mmSocket.connect();
            status = "Connected";
            Log.d("INFO", "Połączono z serwerem!");
            connected = true;
            BufferedReader in = new BufferedReader(new InputStreamReader(mmSocket.getInputStream()));
            String input = in.readLine();
            Log.d("INFO", "Serwer mówi: " + input);
        } catch (IOException e) {}
    }

    public String getStatus() {
        return status;
    }

    public void disconnect() throws IOException {
        status = "Disconnected";
        connected = false;
        if(mmSocket != null)
            mmSocket.close();
        Log.d("HERE", "GOT EXCEPTION: DISCONNECTED!");
    }

    public boolean isMsgDecoded() {
        return msgDecoded;
    }

    public void getFirstToken(TokenValue token) {
        token.set(firstToken.get());
        firstToken.set(NULL);
    }

    public void getSecondToken(TokenValue token) {
        token.set(secondToken.get());
        secondToken.set(NULL);
    }

    public void getFirstStringToken(StringValue stingValue) {
        stingValue.set(msgString1.get());
    }

    public void getSecondStringToken(StringValue stingValue) {
        stingValue.set(msgString2.get());
    }


    public void clearTokens() {
        firstToken.set(NULL);
        secondToken.set(NULL);
    }

    public BluetoothSocket getSocket() {
        return mmSocket;
    }

}

/*
public class ClientBluetoothService extends Service {
    private Handler handler = new Handler();

    private final IBinder mBinder = new LocalBinder();

    private ClientBluetooth clientBT;

    public class LocalBinder extends Binder {
        ClientBluetoothService getService() {
            return ClientBluetoothService.this;
        }
    }
    public ClientBluetoothService() {
        clientBT = new ClientBluetooth();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void generateToast() {
        Log.d("HERE", "Service: generateToast()");
        Toast.makeText(getApplicationContext(), "WITAJ W SERWISIE", Toast.LENGTH_SHORT).show();
    }

    public void setDevice(BluetoothDevice device) {
        clientBT.setDevice(device);
    }

    public void sendCommand(String command) {
        clientBT.send_command(command);
    }

    public void newClient() {
        clientBT = new ClientBluetooth();
    }
    public void start() {
        clientBT.start();
    }

    public String getStatus() {
        return clientBT.getStatus();
    }

    public void disconnect() throws IOException {
        clientBT.disconnect();
    }

    public boolean isMsgDecoded() {
        return clientBT.isMsgDecoded();
    }

    public void getFirstToken(TokenValue token) {
        clientBT.getFirstToken(token);
    }

    public void getSecondToken(TokenValue token) {
        clientBT.getSecondToken(token);
    }

    public void getFirstStringToken(StringValue stingValue) {
        clientBT.getFirstStringToken(stingValue);
    }

    public void clearTokens() {
        clientBT.clearTokens();
    }

    boolean sendComandAndWait(String command) {
        //return clientBT.sendComandAndWait(command);
        return false;
    }

    public BluetoothSocket getSocket() {
        return clientBT.getSocket();
    }

    public void getSecondStringToken(StringValue stingValue) {
        clientBT.getSecondStringToken(stingValue);
    }
}
*/

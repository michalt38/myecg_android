package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 31.10.2017.
 */

public class File {

    private int id;
    private String name;

    public File(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

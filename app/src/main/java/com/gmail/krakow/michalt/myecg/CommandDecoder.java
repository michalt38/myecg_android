package com.gmail.krakow.michalt.myecg;

import java.io.Serializable;
import java.util.Arrays;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.CharState.DELIMITER;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.CharState.TOKEN;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.ERR;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.GET_ECG;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.GET_FILE;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.GET_UPLOAD_STATUS;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.GET_VALUES;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.HELLO;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.OK;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.SAVE_ON_SD;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.UNMOUNT_SD;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.UPLOAD;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.KeywordCode.WRONG_TOKEN;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._ERR;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.TokenType.KEYWORD;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.TokenType.NUMBER;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.TokenType.STRING;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.eReceiverStatus.EMPTY;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.eReceiverStatus.READY;
import static com.gmail.krakow.michalt.myecg.CommandDecoder.eReceiverStatus._OVERFLOW;

/**
 * Created by Michał on 10.09.2017.
 */

public class CommandDecoder implements Serializable{

    public final int RECEIVER_SIZE = 1280;
    public final char RECEIVER_TERMINATOR = 10;

    public final int MAX_KEYWORD_STRING_LTH = 13;
    public final int MAX_KEYWORD_NR = 28;
    public final int MAX_TOKEN_NR = 3;
    public final int SPACE = 32;

    public enum eReceiverStatus {EMPTY, READY, _OVERFLOW};
    public enum TokenType {NULL, KEYWORD, NUMBER, STRING};
    public enum Result {_OK, _ERR};
    public enum KeywordCode {NULL, HELLO, OK, ERR, GET_ECG, SAVE_ON_SD, WRONG_TOKEN, UNMOUNT_SD, GET_FILE, UPLOAD, GET_UPLOAD_STATUS, GET_VALUES};

    private char ucTokenNr;

    class ReceiverBuffer implements Serializable{
        public char[] cData = new char[RECEIVER_SIZE];
        public char ucCharCtr;
        public eReceiverStatus eStatus;
    }

    class TokenValue implements Serializable{
        KeywordCode eKeyword;
        Value uiNumber;
        char [] pcString = new char[2560];
    }

    ReceiverBuffer eReceiverBuffer = new ReceiverBuffer();

    class Keyword implements Serializable
    {
        KeywordCode eCode;
        //char []cString = new char[MAX_KEYWORD_STRING_LTH + 1];
        String cString;

        public Keyword(KeywordCode Code, String Str)
        {eCode = Code; cString = Str;}
    }

    Keyword [] asKeywordList = {
            new Keyword(HELLO, "Hi!!!"),
            new Keyword(GET_ECG, "GET_ECG"),
            new Keyword(OK, "OK"),
            new Keyword(ERR, "ERR"),
            new Keyword(SAVE_ON_SD, "SAVE_ON_SD"),
            new Keyword(WRONG_TOKEN, "WRONG_TOKEN"),
            new Keyword(UNMOUNT_SD, "UNMOUNT_SD"),
            new Keyword(GET_FILE, "GET_FILE"),
            new Keyword(UPLOAD, "UPLOAD"),
            new Keyword(GET_UPLOAD_STATUS, "GET_UPLOAD_STATUS"),
            new Keyword(GET_VALUES, "GET_VALUES")
    };

    class Token implements Serializable
    {
        TokenType eType;
        TokenValue uValue;

        Token() {
            eType = TokenType.NULL;
            uValue = new TokenValue();
        }
    }

    Token [] asToken;

    public CommandDecoder() {
        asToken = new Token[MAX_TOKEN_NR];

        for(int i = 0; i < asToken.length; i++) {
            asToken[i] = new Token();
        }
    }

    enum CharState {DELIMITER, TOKEN};

    public Result eToken_GetKeywordCode(int ucIndex, com.gmail.krakow.michalt.myecg.TokenValue eKey)
    {
        if ((0 != ucTokenNr) && (KEYWORD == asToken[ucIndex].eType))
        {
            eKey.set(asToken[ucIndex].uValue.eKeyword);
            return _OK;
        }
        else
            return _ERR;
    }

    public Result eToken_GetNumber(char ucIndex,  Value uiValue)
    {
        if ((0 != ucTokenNr) && (NUMBER == asToken[ucIndex].eType))
        {
            uiValue.set(asToken[ucIndex].uValue.uiNumber.get());
            return _OK;
        }
        else
            return _ERR;
    }

    public Result eToken_GetString(int ucIndex, StringValue ucDestination)
    {
        if ((0 != ucTokenNr) && (STRING == asToken[ucIndex].eType))
        {
            String str = "";
            for(int i = 0; 0 != asToken[ucIndex].uValue.pcString[i]; i++)
                str += asToken[ucIndex].uValue.pcString[i];
            ucDestination.set(str);
            //Log.d("HERE", "GetString: " + asToken[ucIndex].uValue.pcString[0]);
            //Log.d("HERE", "GetString: " + asToken[ucIndex].uValue.pcString.toString());
            //Log.d("HERE", "GetString: " + ucDestination.get());
            //for(int i = 0; i <asToken[ucIndex].uValue.pcString.length; i++)
            //    asToken[ucIndex].uValue.pcString[i] = 0;
            Arrays.fill(asToken[ucIndex].uValue.pcString, (char)0);
            return _OK;
        }
        else
            return _ERR;
    }

    private char ucFindTokensInString(char []pcString)
    {
        char []cTokens = Arrays.copyOf(pcString,pcString.length+1);
        cTokens[cTokens.length - 1] = 0;
        char ucCharacterCounter;
        char ucTokenAmount = 0;
        CharState eCharState = TOKEN;
        CharState ePreviousCharState = DELIMITER;

        for (ucCharacterCounter = 0; ; ucCharacterCounter++)
        {
            switch (eCharState)
            {
                case TOKEN:
                    if ((MAX_TOKEN_NR <= ucTokenNr) || (0 == cTokens[ucCharacterCounter]))
                    {
                        return ucTokenAmount;
                    }
                    else if (SPACE == cTokens[ucCharacterCounter])
                    {
                        eCharState = DELIMITER;
                        ePreviousCharState = TOKEN;
                    }
                    else if (DELIMITER == ePreviousCharState)
                    {
                        char cCharacterCounter;
                        int cTokensCounter = ucCharacterCounter;
                        //CopyString(&cTokens[ucCharacterCounter], asToken[ucTokenAmount++].uValue.pcString);
                        for(cCharacterCounter = 0; 0 != cTokens[cTokensCounter]; cCharacterCounter++)
                        {
                            asToken[ucTokenAmount].uValue.pcString[cCharacterCounter] = cTokens[cTokensCounter++];
                        }
                        asToken[ucTokenAmount].uValue.pcString[cCharacterCounter] = 0;
                        ucTokenAmount++;
                        //asToken[ucTokenAmount++].uValue.pcString = Arrays.copyOf(cTokens[ucCharacterCounter]);
                        ePreviousCharState = TOKEN;
                    }
                    break;
                case DELIMITER:
                    if (0 == cTokens[ucCharacterCounter])
                    {
                        return ucTokenAmount;
                    }
                    else if (SPACE != cTokens[ucCharacterCounter])
                    {
                        eCharState = TOKEN;
                        ePreviousCharState = DELIMITER;
                        ucCharacterCounter--;
                    }
                    break;
            }
        }
    }

    private Result eStringKeyword(char pcStrChar[], com.gmail.krakow.michalt.myecg.TokenValue peKeywordCode)
    {
        char ucKeywordNr;
        String pcStr = "";
        for(int i = 0; pcStrChar[i] != 0; i++)
            pcStr += pcStrChar[i];

        for (ucKeywordNr = 0; asKeywordList.length > ucKeywordNr; ucKeywordNr++)
        {
            if (pcStr.equals(asKeywordList[ucKeywordNr].cString))
            {
                peKeywordCode.set(asKeywordList[ucKeywordNr].eCode);
                return _OK;
            }
        }
        return _ERR;
    };

    private void DecodeTokens()
    {
        char ucTokenCounter;
        Value uiHexValue = new Value();
        com.gmail.krakow.michalt.myecg.TokenValue eKeyValue = new com.gmail.krakow.michalt.myecg.TokenValue();

        for (ucTokenCounter = 0; ucTokenCounter < ucTokenNr; ucTokenCounter++)
        {
            if (_OK == eStringKeyword(asToken[ucTokenCounter].uValue.pcString, eKeyValue))
            {
                asToken[ucTokenCounter].uValue.eKeyword = eKeyValue.get();
                asToken[ucTokenCounter].eType = KEYWORD;
            }
            else if (_OK == eHexStringToUInt(asToken[ucTokenCounter].uValue.pcString, uiHexValue))
            {
                asToken[ucTokenCounter].uValue.uiNumber = uiHexValue;
                asToken[ucTokenCounter].eType = NUMBER;
            }
            else
            {
                asToken[ucTokenCounter].eType = STRING;
            }
        }
    }

    public void DecodeMsg(StringValue pcStringValue)
    {
        String pcString = pcStringValue.get();
        char ucIndex;

        for (ucIndex = 0; ucIndex < MAX_TOKEN_NR; ucIndex++)
        {
            asToken[ucIndex].eType = STRING;
        }

        char ucTokenCounter;

        ucTokenNr = ucFindTokensInString(pcString.toCharArray());
        for (ucTokenCounter = 0; ucTokenNr > ucTokenCounter; ucTokenCounter++)
        {
            /*
            for(int j = 0; j < asToken[ucTokenCounter].uValue.pcString.length; j++) {
                if(asToken[ucTokenCounter].uValue.pcString[j] == SPACE)
                    asToken[ucTokenCounter].uValue.pcString[j] = 0;
            }
            */
            int count = 0;
            while(0 != asToken[ucTokenCounter].uValue.pcString[count]) {
                if(asToken[ucTokenCounter].uValue.pcString[count] == SPACE)
                    asToken[ucTokenCounter].uValue.pcString[count] = 0;
                count++;
            }
            //ReplaceCharactersInString(asToken[ucTokenCounter].uValue.pcString, SPACE, 0);
        }
        DecodeTokens();
    }

    public void Receiver_PutCharacterToBuffer(char cCharacter)
    {
        if (RECEIVER_SIZE > eReceiverBuffer.ucCharCtr)
        {
            if (RECEIVER_TERMINATOR == cCharacter)
            {
                eReceiverBuffer.cData[eReceiverBuffer.ucCharCtr++] = 0;
                eReceiverBuffer.eStatus = READY;
                eReceiverBuffer.ucCharCtr = 0;
            }
            else
            {
                eReceiverBuffer.cData[eReceiverBuffer.ucCharCtr++] = cCharacter;
            }
        }
        else
        {
            eReceiverBuffer.eStatus = _OVERFLOW;
        }
    }

    public eReceiverStatus eReciver_GetStatus()
    {
        return eReceiverBuffer.eStatus;
    }

    public void Receiver_GetStringCopy(StringValue ucDestination)
    {
        String tmp = "";
        for(int i = 0; 0 != eReceiverBuffer.cData[i]; i++) tmp += eReceiverBuffer.cData[i];
        ucDestination.set(tmp);
        //CopyString(eReceiverBuffer.cData, ucDestination);
        eReceiverBuffer.eStatus = EMPTY;
    }

    private Result eHexStringToUInt(char pcStr[], Value puiValue)
    {
        int ucMaxPower = -1;
        char ucCharacterCounter;
        char ucTestedCharacter;
        char ucTestPassed;
        int ucFactor;
        char ucRepetitions;

        if(('0' != pcStr[0]) || ('x' != pcStr[1]) || (0 == pcStr[2]))
        {
            return _ERR;
        }
        for(ucCharacterCounter = 2; 0 != pcStr[ucCharacterCounter]; ucCharacterCounter++)
        {
            ucTestPassed = 0;
            for(ucTestedCharacter = '0'; 'F' >= ucTestedCharacter; ucTestedCharacter++)
            {
                if(pcStr[ucCharacterCounter] == ucTestedCharacter)
                {
                    ucTestPassed = 1;
                    break;
                }
                if('9' == ucTestedCharacter)
                {
                    ucTestedCharacter = 'A';
                    ucTestedCharacter--;
                }
            }
            if(0 == ucTestPassed)
            {
                return _ERR;
            }
            if(3 < ++ucMaxPower)
            {
                return _ERR;
            }
        }
    puiValue.set(0);
        for(ucCharacterCounter = 2; 0 != pcStr[ucCharacterCounter]; ucCharacterCounter++)
        {
            ucFactor = 1;
            for(ucRepetitions = (char)ucMaxPower; ucRepetitions > 0; ucRepetitions--)
            {
                ucFactor *= 16;
            }
            if('0' <= pcStr[ucCharacterCounter] && '9' >= pcStr[ucCharacterCounter])
            {
            puiValue.set(puiValue.get() + (pcStr[ucCharacterCounter] - '0') * ucFactor);
            }
            if('A' <= pcStr[ucCharacterCounter] && 'F' >= pcStr[ucCharacterCounter])
            {
            puiValue.set(puiValue.get() + (pcStr[ucCharacterCounter] - ('A' - 10)) * ucFactor);
            }
            ucMaxPower--;
        }
        return _OK;

    };
}

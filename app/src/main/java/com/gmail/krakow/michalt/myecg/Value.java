package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 10.09.2017.
 */

public class Value {
    private int value;

    public Value() {
        value = 0;
    }

    public void set(int val) {
        value = val;
    }

    public int get() {
        return value;
    }
}

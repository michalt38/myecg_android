package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import dmax.dialog.SpotsDialog;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

public class GetFilesToDrawActivity extends AppCompatActivity {

    FillListView fillListView;

    private ListView filesListView;
    private List<File> filesListIteams;
    private FilesAdapter filesListAdapter;

    CommandDecoder commandDecoder;
    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;
    BluetoothSocket mmSocket;

    SpotsDialog dialog;
    Timer timer;

    int i;
    List<Integer> data_values;
    List<Float> data_times;


    @Override
    protected void onStart() {
        super.onStart();
        mmSocket = ((MyECG) this.getApplication()).getSocket();
        if(!filesListIteams.isEmpty()) return;
        dialog = new SpotsDialog(GetFilesToDrawActivity.this, R.style.Loading);
        dialog.show();
        fillListView = new FillListView(String.valueOf(((MyECG) this.getApplication()).getUserId()));
        fillListView.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mmSocket = null;
        fillListView = null;
        data_values.clear();
        data_times.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_files_to_draw);
        setTitle("Select file");
        commandDecoder = new CommandDecoder();
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
        i = 0;
        filesListView = (ListView)findViewById(R.id.filesListView);
        filesListIteams = new ArrayList<>();
        data_values = new ArrayList<>();
        data_times = new ArrayList<>();
    }

    class FillListView extends Thread {

        private int i;
        private String userID;

        FillListView(String id) {
            i = 0;
            userID = id;
        }

        @Override
        public void run() {
            super.run();

            while (true) {
                sendMsg("GET_FILE " + userID);
                if (firstToken.get() == CommandDecoder.KeywordCode.GET_FILE) {
                    if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                        filesListIteams.add(new File(i++, msgString2.get()));
                    } else {
                        setList();
                        break;
                    }
                } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                    sendMsg("GET_FILE " + userID);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            });
        }

        private void sendMsg(String msg) {
            commandDecoder = new CommandDecoder();
            String input = null;
            try {
                BluetoothSocket socket = mmSocket;
                if (socket == null) {

                    //Log.d("HERE", "NULL socket");
                    return;
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println(msg);
                input = in.readLine();
                Log.d("HERE", "Input: " + input);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TokenValue token = new TokenValue();
            TokenValue token2 = new TokenValue();
            StringValue stringToken = new StringValue();
            StringValue stringToken2 = new StringValue();

            for (int i = 0; i < input.length(); i++) {
                commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
            }
            commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
            if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
                StringValue received_msg = new StringValue();
                commandDecoder.Receiver_GetStringCopy(received_msg);
                commandDecoder.DecodeMsg(received_msg);

                if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                    firstToken.set(token.get());

                    if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                        secondToken.set(token2.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                        msgString1.set(stringToken.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                        msgString2.set(stringToken2.get());
                    }
                }
            }
        }

        public void setList() {
            filesListAdapter = new FilesAdapter(getApplicationContext(), filesListIteams);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    filesListView.setAdapter(filesListAdapter);
                    setOnListViewIteamClickListener();
                }
            });
        }

        public void setOnListViewIteamClickListener() {
            filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.d("HERE", "HERE");
                    String fileName = filesListIteams.get(i).getName();
                    dialog = new SpotsDialog(GetFilesToDrawActivity.this, R.style.Loading);
                    dialog.show();
                    GetValues getValues = new GetValues(userID, fileName);
                    getValues.start();

                }
            });
        }
    }

    class GetValues extends Thread {
        private String file_name;
        private String id;

        GetValues(String id, String file) {
            this.id = id;
            file_name = file;
        }

        @Override
        public void run() {
            super.run();


            String date_time = "";
            date_time = file_name.substring(file_name.indexOf("_"));
            //file_name = file_name.replace("_", " ");
            StringBuilder buildString = new StringBuilder(date_time);
            buildString.setCharAt(14, ':');
            buildString.setCharAt(17, ':');
            date_time = buildString.toString();
            date_time = date_time.substring(1, 20);
            Log.d("HERE", date_time);
            firstToken = new TokenValue();
            secondToken = new TokenValue();
            msgString1 = new StringValue();
            msgString2 = new StringValue();
            float time = 0;
            while (true) {
                sendMsg("GET_VALUES " + file_name);
                Log.d("HERE", "GET_VALUES " + file_name);
                if (firstToken.get() == CommandDecoder.KeywordCode.GET_VALUES) {
                    if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                        String values = msgString2.get();
                        Log.d("HERE", "Values read: " + values);
                        for (String s : values.split(",")) {
                            //data.add(Integer.parseInt(s));
                            String s1[] = s.split("_");
                            time += Integer.parseInt(s1[0])/1000;
                            data_times.add(time);
                            data_values.add(Integer.parseInt(s1[1]));
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                Intent intent = new Intent(GetFilesToDrawActivity.this, DrawGraphActivity.class);
                                intent.putIntegerArrayListExtra("data_values", (ArrayList<Integer>) data_values);
                                intent.putExtra("data_times", (Serializable) data_times);
                                //intent.putIntegerArrayListExtra("data_times", (ArrayList<Integer>) data_times);
                                startActivity(intent);
                                //Toast.makeText(getApplicationContext(), "Uploading done", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    }
                } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                    sendMsg("GET_VALUES " + file_name);
                }
            }

        }

        private void sendMsg(String msg) {
            commandDecoder = new CommandDecoder();
            String input = null;
            try {
                BluetoothSocket socket = mmSocket;
                if (socket == null) {

                    Log.d("HERE", "NULL socket");
                    return;
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println(msg);
                input = in.readLine();
                Log.d("HERE", "Input: " + input);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TokenValue token = new TokenValue();
            TokenValue token2 = new TokenValue();
            StringValue stringToken = new StringValue();
            StringValue stringToken2 = new StringValue();

            for (int i = 0; i < input.length(); i++) {
                commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
            }
            commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
            if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
                StringValue received_msg = new StringValue();
                //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
                commandDecoder.Receiver_GetStringCopy(received_msg);
                commandDecoder.DecodeMsg(received_msg);

                if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                    firstToken.set(token.get());

                    if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                        secondToken.set(token2.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                        msgString1.set(stringToken.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                        msgString2.set(stringToken2.get());
                    }
                }
            }
        }
    }
}

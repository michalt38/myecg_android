package com.gmail.krakow.michalt.myecg;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Michał on 11.02.2018.
 */

public class SignalDateAdapter extends BaseAdapter{

    private Context mContext;
    private List<SignalDate> mDatesList;

    public SignalDateAdapter(Context context, List<SignalDate> datesList) {
        mContext = context;
        mDatesList = datesList;
    }

    @Override
    public int getCount() {
        return mDatesList.size();
    }

    @Override
    public Object getItem(int i) {
        return mDatesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.signal_dates_list, null);
        TextView fileName = (TextView)v.findViewById(R.id.signal_date);

        fileName.setText(mDatesList.get(i).getDate());

        v.setTag(mDatesList.get(i).getId());
        return v;
    }
}

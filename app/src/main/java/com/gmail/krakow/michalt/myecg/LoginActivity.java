package com.gmail.krakow.michalt.myecg;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity implements AsyncResponse{

    EditText emailEditText, passwordEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailEditText = (EditText)findViewById(R.id.emailEditText);
        passwordEditText = (EditText)findViewById(R.id.passwordEditText);
        passwordEditText.setTypeface(Typeface.DEFAULT);
        passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
    }

    public void onLogin(View view) {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String type = "login";
        BackgroundWorker backgroundWorker = new BackgroundWorker(this);
        backgroundWorker.delegate = this;
        backgroundWorker.execute(type, email, password);
    }

    @Override
    public void getUserData(int id, String name, String surname) {
        Log.d("HERE", "GET USER DATA");
        ((MyECG) this.getApplication()).setUserId(id);
        ((MyECG) this.getApplication()).setUserName(name);
        ((MyECG) this.getApplication()).setUserSurname(surname);
        Intent intent = new Intent();
        intent.putExtra("USER", name + " " + surname);
        setResult(RESULT_OK, intent);
        finish();
    }
}

package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import dmax.dialog.SpotsDialog;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

public class UploadActivity extends AppCompatActivity {

    //protected boolean mBound = false;
    //private ClientBluetoothService clientBluetoothService;

    FillListView fillListView;

    private ListView filesListView;
    private List<File> filesListIteams;
    private FilesAdapter filesListAdapter;

    CommandDecoder commandDecoder;
    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;
    BluetoothSocket mmSocket;

    SpotsDialog dialog;
    Timer timer;

    int i;

    @Override
    protected void onStart() {
        super.onStart();
        mmSocket = ((MyECG) this.getApplication()).getSocket();
        if(!filesListIteams.isEmpty()) return;
        dialog = new SpotsDialog(UploadActivity.this, R.style.Listing);
        dialog.show();
        fillListView = new FillListView(String.valueOf(((MyECG) this.getApplication()).getUserId()));
        fillListView.start();

        //Intent intent = new Intent(this, ClientBluetoothService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //if (mBound) {
        //    unbindService(mConnection);
        //    mBound = false;
        //}
        mmSocket = null;
        fillListView = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        setTitle("Select file");
        commandDecoder = new CommandDecoder();
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
        i = 0;
        filesListView = (ListView)findViewById(R.id.filesListView);
        filesListIteams = new ArrayList<>();
    }

    /*
    public void setList() {
        filesListAdapter = new FilesAdapter(getApplicationContext(), filesListIteams);
        filesListView.setAdapter(filesListAdapter);
    }
    */


    class GetValues extends Thread{
        private String file_name;
        private String id;

        GetValues(String id, String file) {
            this.id = id;
            file_name = file;
        }

        @Override
        public void run() {
            super.run();


            String date_time = "";
            date_time = file_name.substring(file_name.indexOf("_"));
            //file_name = file_name.replace("_", " ");
            StringBuilder buildString = new StringBuilder(date_time);
            buildString.setCharAt(14, ':');
            buildString.setCharAt(17, ':');
            date_time = buildString.toString();
            date_time = date_time.substring(1, 20);
            Log.d("HERE", date_time);
            //BackgroundWorker worker = new BackgroundWorker(getApplicationContext());
            //worker.execute("add_signal", id, date_time);
            addSignal(id, date_time);

            while(true) {
                sendMsg("GET_VALUES " + file_name);
                if (firstToken.get() == CommandDecoder.KeywordCode.GET_VALUES) {
                    if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                        String values = msgString2.get();
                        Log.d("HERE", "Values read: " + values);
                        String post_data;
                        try {
                            post_data = URLEncoder.encode("time", "UTF-8") + "[]=";
                            values = values.replaceAll(",","&" + URLEncoder.encode("time", "UTF-8") + "[]=");
                            values = values.replaceAll("_","&" + URLEncoder.encode("value", "UTF-8") + "[]=");
                            post_data += values;
                            addValue(post_data);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        //addValue(msgString2.get());
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Uploading done", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                    }
                } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                    sendMsg("GET_VALUES " + file_name);
                }
            }

        }

        private void sendMsg(String msg) {
            commandDecoder = new CommandDecoder();
            String input = null;
            try {
                BluetoothSocket socket = mmSocket;
                if (socket == null) {

                    //Log.d("HERE", "NULL socket");
                    return;
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println(msg);
                input = in.readLine();
                Log.d("HERE", "Input: " + input);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TokenValue token = new TokenValue();
            TokenValue token2 = new TokenValue();
            StringValue stringToken = new StringValue();
            StringValue stringToken2 = new StringValue();

            for (int i = 0; i < input.length(); i++) {
                commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
            }
            commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
            if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
                StringValue received_msg = new StringValue();
                //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
                commandDecoder.Receiver_GetStringCopy(received_msg);
                commandDecoder.DecodeMsg(received_msg);

                if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                    firstToken.set(token.get());

                    if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                        secondToken.set(token2.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                        msgString1.set(stringToken.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                        msgString2.set(stringToken2.get());
                    }
                }
            }
        }

        public void addSignal(String user, String date) {
            String add_signal_url = "http://student.agh.edu.pl/~milchalt/myECG/add_signal.php";
            try {
                String str_url = add_signal_url+"?user="+user+"&date="+date;
                URL url = new URL(str_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String result = "";
                String line = "";
                while((line = bufferedReader.readLine()) != null) {
                    result += line + "\n";
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void addValue(String post_data) {
            String add_signal_url = "http://student.agh.edu.pl/~milchalt/myECG/add.php";
            try {
                String str_url = add_signal_url + "?" + post_data;
                URL url = new URL(str_url);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line + "\n";
                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class FillListView extends Thread {

        private int i;
        private String userID;
        FillListView(String id) {
            i = 0;
            userID = id;
        }

        @Override
        public void run() {
            super.run();

            while(true) {
                sendMsg("GET_FILE " + userID);
                //if(clientBluetoothService.isMsgDecoded()) {
                if (firstToken.get() == CommandDecoder.KeywordCode.GET_FILE) {
                    if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                        filesListIteams.add(new File(i++, msgString2.get()));
                    } else {
                        setList();
                        break;
                    }
                } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                    sendMsg("GET_FILE " + userID);
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            });
        }

        private void sendMsg(String msg) {
            commandDecoder = new CommandDecoder();
            String input = null;
            try {
                BluetoothSocket socket = mmSocket;
                if (socket == null) {

                    //Log.d("HERE", "NULL socket");
                    return;
                }
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println(msg);
                input = in.readLine();
                Log.d("HERE", "Input: " + input);
            } catch (IOException e) {
                e.printStackTrace();
            }

            TokenValue token = new TokenValue();
            TokenValue token2 = new TokenValue();
            StringValue stringToken = new StringValue();
            StringValue stringToken2 = new StringValue();

            for (int i = 0; i < input.length(); i++) {
                commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
            }
            commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
            if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
                StringValue received_msg = new StringValue();
                //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
                commandDecoder.Receiver_GetStringCopy(received_msg);
                commandDecoder.DecodeMsg(received_msg);

                if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                    firstToken.set(token.get());

                    if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                        secondToken.set(token2.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                        msgString1.set(stringToken.get());
                    }

                    if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                        msgString2.set(stringToken2.get());
                    }
                }
            }
        }

        public void setList() {
            filesListAdapter = new FilesAdapter(getApplicationContext(), filesListIteams);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    filesListView.setAdapter(filesListAdapter);
                    setOnListViewIteamClickListener();
                }
            });
        }

        public void checkUploadingStatus() {
            sendMsg("GET_UPLOAD_STATUS");
            if (firstToken.get() == CommandDecoder.KeywordCode.GET_UPLOAD_STATUS) {
                if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                    String status = msgString2.get();
                    if(status.equals("100")) {
                        dialog.dismiss();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "UPLOADING DONE", Toast.LENGTH_SHORT).show();
                            }
                        });
                        timer.cancel();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT).show();
                }
            }
        }

        public void setOnListViewIteamClickListener() {
            filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Log.d("HERE", "HERE");
                    String fileName = filesListIteams.get(i).getName();
                    //sendMsg("GET_VALUES " + fileName);
                    dialog = new SpotsDialog(UploadActivity.this, R.style.Uploading);
                    dialog.show();
                    GetValues getValues = new GetValues(userID, fileName);
                    getValues.start();
                    /*
                    if (firstToken.get() == CommandDecoder.KeywordCode.UPLOAD) {
                        dialog = new SpotsDialog(UploadActivity.this, R.style.Uploading);
                        dialog.show();
                        TimerTask checkStatus = new TimerTask() {
                            @Override
                            public void run() {
                                checkUploadingStatus();
                            }
                        };

                        timer = new Timer();
                        long delay = 0;
                        long interval = 500;
                        timer.schedule(checkStatus, delay, interval);
                    } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                        Toast.makeText(getApplicationContext(), "WRONG TOKEN", Toast.LENGTH_SHORT).show();
                    }
                    */
                }
            });
        }
    }

    /*
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {

            ClientBluetoothService.LocalBinder binder = (ClientBluetoothService.LocalBinder) service;
            clientBluetoothService = binder.getService();
            mBound = true;
            fillListView = new FillListView();
            //sendMessage("GET_FILE");
            fillListView.start();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
    */

    private void sendMessage(String msg) {
        commandDecoder = new CommandDecoder();
        String input = null;
        try {
            BluetoothSocket socket = mmSocket;
            if(socket == null) {

                //Log.d("HERE", "NULL socket");
                return;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(msg);
            input = in.readLine();
            Log.d("HERE", "Input: " + input);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        TokenValue token = new TokenValue();
        TokenValue token2 = new TokenValue();
        StringValue stringToken = new StringValue();
        StringValue stringToken2 = new StringValue();

        for(int i = 0; i < input.length(); i++) {
            commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
        }
        commandDecoder.Receiver_PutCharacterToBuffer((char)10);
        if(CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
            StringValue received_msg = new StringValue();
            //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
            commandDecoder.Receiver_GetStringCopy(received_msg);
            commandDecoder.DecodeMsg(received_msg);

            if(_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                firstToken.set(token.get());

                if(_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                    secondToken.set(token2.get());
                }

                if(_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                    msgString1.set(stringToken.get());
                }

                if(_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                    msgString2.set(stringToken2.get());
                }
            }
        }


        /*
        if (firstToken.get() == CommandDecoder.KeywordCode.GET_FILE) {
            if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                filesListIteams.add(new File(i++, msgString2.get()));
                Log.d("HERE", "NEW_FILE");
            } else {
                setList();
                Log.d("HERE", "NO_FILES");
                return;
            }
        } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
            sendMessage("GET_FILE");
        }
        sendMessage("GET_FILE");
        */
    }


}

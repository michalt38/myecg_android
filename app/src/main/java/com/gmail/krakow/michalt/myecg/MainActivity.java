package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

public class MainActivity extends AppCompatActivity {

    private ListView dialog_list = null;
    private boolean isStop;
    //private Menu menu;
    private DeviceSettings deviceSettings;
    //private ClientBluetooth clientBT;

    private ListView mainListView;
    private List<MainListIteam> mainListIteams;
    private MainListAdapter mainListAdapter;

    final int CONNECT_DEVICE = 1;

    //protected ClientBluetoothService clientBluetoothService;
    protected boolean mBound = false;

    boolean measurement_running;

    CommandDecoder commandDecoder;
    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;
    BluetoothSocket mmSocket = null;
    BluetoothDevice mmDevice = null;
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();;

    public void setBT(){
        mmDevice = mBluetoothAdapter.getRemoteDevice("58:A8:39:00:9E:12");
        BluetoothSocket tmp = null;
        try {
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            tmp = mmDevice.createRfcommSocketToServiceRecord(uuid);
        } catch (Exception e) {}
        ((MyECG) this.getApplication()).setSocket(tmp);
        //mmSocket = tmp;
        Log.d("INFO", "Próba połączenia....");
        try {
            ((MyECG) this.getApplication()).getSocket().connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("INFO", "Połączono z serwerem!");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(((MyECG) this.getApplication()).getSocket().getInputStream()));
            String input = null;
            input = in.readLine();
        }catch (IOException e) {}

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deviceSettings = new DeviceSettings();
        setContentView(R.layout.activity_main);
        commandDecoder = new CommandDecoder();
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
        //setBT();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();

        measurement_running = false;

        ActionBar ab = getSupportActionBar();
        //ab.setLogo(R.mipmap.ic_launcher);
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayShowHomeEnabled(true);
        ab.setDefaultDisplayHomeAsUpEnabled(true);
        //clientBT = new ClientBluetooth();

        mainListView = (ListView)findViewById(R.id.mainListView);
        mainListIteams = new ArrayList<>();
        mainListIteams.add(new MainListIteam(0, "Start measurement", R.drawable.ic_play_arrow_black_24dp));
        mainListIteams.add(new MainListIteam(1, "Draw signal", R.drawable.ic_show_chart_black_24dp));
        mainListIteams.add(new MainListIteam(2, "Upload to server", R.drawable.ic_file_upload_black_24dp));
        mainListIteams.add(new MainListIteam(3, "Settings", R.drawable.ic_settings_black_24dp));
        mainListAdapter = new MainListAdapter(getApplicationContext(), mainListIteams);
        mainListView.setAdapter(mainListAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                switch (i) {
                    case 0:
                        if(!prefs.getBoolean("connect_device", false)) {
                            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
                            break;
                        }
                        if(!prefs.getBoolean("log_in", false)) {
                            Toast.makeText(getApplicationContext(), "Sign in", Toast.LENGTH_LONG).show();
                            break;
                        }

                        if(!measurement_running) {
                            WaitThreadClass waitThreadClass = new WaitThreadClass(true, getID(), getName());
                            waitThreadClass.start();
                        } else {
                            WaitThreadClass waitThreadClass = new WaitThreadClass(false, getID(), getName());
                            waitThreadClass.start();
                        }
                        //clientBluetoothService.sendCommand("HELLO");
                        /*
                        if(!measurement_running) {
                            String currentDateTimeString = android.text.format.DateFormat.getLongDateFormat(MainActivity.this).getDateTimeInstance().format(new Date());
                            currentDateTimeString = currentDateTimeString.replace(" ", "_").toLowerCase();
                            currentDateTimeString = currentDateTimeString.replace(".", "-").toLowerCase();
                            currentDateTimeString = currentDateTimeString.replace(":", "-").toLowerCase();
                            clientBluetoothService.clearTokens();
                            clientBluetoothService.sendCommand("SAVE_ON_SD ON " + currentDateTimeString + ".csv");



                                    */
/*
                        } else {
                            clientBluetoothService.clearTokens();
                            clientBluetoothService.sendCommand("SAVE_ON_SD OFF");

                            WaitThreadClass waitThreadClass = new WaitThreadClass(false);
                            waitThreadClass.start();


                        }*/
                        break;
                    case 1:
                        /*
                        if(!deviceSettings.isConnected()){
                            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
                            break;
                        }

                        if(!prefs.getBoolean("connect_device", false)) {
                            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
                            break;
                        }
                        if(!prefs.getBoolean("log_in", false)) {
                            Toast.makeText(getApplicationContext(), "Sign in", Toast.LENGTH_LONG).show();
                            break;
                        }
                        */
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.MyDialogTheme);

                        // add a list
                        String[] items = {"Local", "Online"};
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                                Intent intent;
                                switch (which) {
                                    case 0: // local
                                        //Toast.makeText(getApplicationContext(), "Local", Toast.LENGTH_SHORT).show();

                                        if(!prefs.getBoolean("connect_device", false)) {
                                            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
                                            break;
                                        }
                                        if(!prefs.getBoolean("log_in", false)) {
                                            Toast.makeText(getApplicationContext(), "Sign in", Toast.LENGTH_LONG).show();
                                            break;
                                        }
/*
                                        if(measurement_running) {
                                            Toast.makeText(getApplicationContext(), "Stop measurement first", Toast.LENGTH_SHORT).show();
                                            break;
                                        }
*/
                                        intent = new Intent(MainActivity.this, GetFilesToDrawActivity.class);
                                        startActivity(intent);

                                        break;
                                    case 1: // online
                                        //Toast.makeText(getApplicationContext(), "Online", Toast.LENGTH_SHORT).show();

                                        if(!prefs.getBoolean("log_in", false)) {
                                            Toast.makeText(getApplicationContext(), "Sign in", Toast.LENGTH_LONG).show();
                                            break;
                                        }

                                        intent = new Intent(MainActivity.this, ShowDatabaseActivity.class);
                                        startActivity(intent);
                                        break;
                                }
                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();

                        /*
                        intent = new Intent(MainActivity.this, MeasureActivity.class);
                        Log.d("HERE", "Start Measure Intent " + deviceSettings.getSaveSignal());
                        intent.putExtra("SAVE_ON_SD", deviceSettings.getSaveSignal());
                        startActivity(intent);
                        */
                        break;
                    case 2:
                        if(!prefs.getBoolean("connect_device", false)) {
                            Toast.makeText(getApplicationContext(), "Device is disconnected", Toast.LENGTH_LONG).show();
                            break;
                        }
                        if(!prefs.getBoolean("log_in", false)) {
                            Toast.makeText(getApplicationContext(), "Sign in", Toast.LENGTH_LONG).show();
                            break;
                        }

                        /*
                        if(measurement_running) {
                            Toast.makeText(getApplicationContext(), "Stop measurement first", Toast.LENGTH_SHORT).show();
                            break;
                        }
                        */
                        intent = new Intent(MainActivity.this, UploadActivity.class);
                        startActivity(intent); 
                        break;
                    case 3:
                        intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                }
            }
        });
    }

    private String getID() {return String.valueOf(((MyECG) this.getApplication()).getUserId());}
    private String getName() {return ((MyECG) this.getApplication()).getUserName() + "_" + ((MyECG) this.getApplication()).getUserSurname();}

    private void sendMessage(String msg) {
        String input = null;
        try {
            BluetoothSocket socket = ((MyECG) this.getApplication()).getSocket();
            if(socket == null) {

                //Log.d("HERE", "NULL socket");
                return;
            }
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(msg);
            input = in.readLine();
            Log.d("HERE", "Input: " + input);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        TokenValue token = new TokenValue();
        TokenValue token2 = new TokenValue();
        StringValue stringToken = new StringValue();
        StringValue stringToken2 = new StringValue();

        for(int i = 0; i < input.length(); i++) {
            commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
        }
        commandDecoder.Receiver_PutCharacterToBuffer((char)10);
        if(CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
            StringValue received_msg = new StringValue();
            //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
            commandDecoder.Receiver_GetStringCopy(received_msg);
            commandDecoder.DecodeMsg(received_msg);

            if(_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                firstToken.set(token.get());

                if(_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                    secondToken.set(token2.get());
                }

                if(_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                    msgString1.set(stringToken.get());
                }

                if(_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                    msgString2.set(stringToken2.get());
                }
            }
        }
    }

    private class WaitThreadClass extends Thread{
        private boolean isStop;
        private boolean isOn;
        private String id;
        private String name;

        public WaitThreadClass(boolean on, String user_id, String user_name) {
            isStop = false;
            isOn = on;
            id = user_id;
            name = user_name;
        }

        public void run() {
            if(isOn) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
                String currentDateTimeString = df.format(Calendar.getInstance().getTime());
                Log.d("HERE", "Date: " + currentDateTimeString);
                //String currentDateTimeString = android.text.format.DateFormat.getDateFormat(getApplicationContext()).format(new Date("yyyy.MM.dd hh:mm:ss"));
                currentDateTimeString = currentDateTimeString.replace(" ", "_").toLowerCase();
                currentDateTimeString = currentDateTimeString.replace(".", "-").toLowerCase();
                currentDateTimeString = currentDateTimeString.replace(":", "-").toLowerCase();
                currentDateTimeString = id + "_" + currentDateTimeString;
                sendMessage("SAVE_ON_SD ON " + currentDateTimeString + ".txt" + " " + name);

                //TokenValue tokenValue1 = new TokenValue();
                //TokenValue tokenValue2 = new TokenValue();
                //while(true) {
                    //if(clientBluetoothService.isMsgDecoded()) {
                        //clientBluetoothService.getFirstToken(tokenValue1);
                        //clientBluetoothService.getSecondToken(tokenValue2);
                        if(firstToken.get() == CommandDecoder.KeywordCode.SAVE_ON_SD) {
                            if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mainListIteams.get(0).setText("Stop Measurement");
                                        mainListIteams.get(0).setImage(R.drawable.ic_pause_black_24dp);
                                        mainListAdapter = new MainListAdapter(getApplicationContext(), mainListIteams);
                                        mainListView.setAdapter(mainListAdapter);
                                        Toast.makeText(getApplicationContext(), "Measurement running", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                measurement_running = true;
                                //break;
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT);
                                    }
                                });
                                //break;
                            }
                        } else if(firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "WRONG TOKEN", Toast.LENGTH_SHORT);
                                }
                            });
                            //break;
                        }
                    //}
                //}
            }else {
                //TokenValue tokenValue1 = new TokenValue();
                //TokenValue tokenValue2 = new TokenValue();
                //while(true) {
                    //if(clientBluetoothService.isMsgDecoded()) {
                        //clientBluetoothService.getFirstToken(tokenValue1);
                        //clientBluetoothService.getSecondToken(tokenValue2);
                sendMessage("SAVE_ON_SD OFF");
                        if(firstToken.get() == CommandDecoder.KeywordCode.SAVE_ON_SD) {
                            if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mainListIteams.get(0).setText("Start Measurement");
                                        mainListIteams.get(0).setImage(R.drawable.ic_play_arrow_black_24dp);
                                        mainListAdapter = new MainListAdapter(getApplicationContext(), mainListIteams);
                                        mainListView.setAdapter(mainListAdapter);
                                        Toast.makeText(getApplicationContext(), "Measurement stopped", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                measurement_running = false;
                                //break;
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                //break;
                            }
                        } else if (firstToken.get() == CommandDecoder.KeywordCode.WRONG_TOKEN) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, "WRONG TOKEN", Toast.LENGTH_SHORT).show();
                                }
                            });
                            //break;
                        }
                    //}
                //}
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Intent intent = new Intent(this, ClientBluetoothService.class);
        //bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //if (mBound) {
        //    unbindService(mConnection);
        //    mBound = false;
        //}
    }

    public void disconnect() throws IOException {
        //clientBluetoothService.disconnect();
    }

    /*
    public void start(View v){
        if (mBound) {
            //clientBluetoothService.generateToast();
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            BluetoothDevice server = mBluetoothAdapter.getRemoteDevice("58:A8:39:00:9E:12");
            clientBluetoothService.setDevice(server);
            clientBluetoothService.start();
        }
    }
    */

/*
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            ClientBluetoothService.LocalBinder binder = (ClientBluetoothService.LocalBinder) service;
            clientBluetoothService = binder.getService();
            mBound = true;
            mmSocket = clientBluetoothService.getSocket();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };*/

    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.bluetooth_id:
                //Toast.makeText(getApplicationContext(), "Bluetooth icon is selected", Toast.LENGTH_SHORT).show();
                if(!deviceSettings.isConnected())
                {
                    Intent intent = new Intent(this, ConnectDeviceActivity.class);
                    intent.putExtra("deviceSettings", deviceSettings);
                    //intent.putExtra("clientBT", clientBT);
                    startActivityForResult(intent, CONNECT_DEVICE);
                }
                else
                {
                    try {
                        clientBluetoothService.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    deviceSettings.setConnected(false);
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_bluetooth_connected_white_24dp));
                    menu.getItem(0).setTitle("Connect device");

                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case CONNECT_DEVICE:
                if(resultCode == RESULT_OK) {
                    deviceSettings.setConnected(true);
                    //clientBT = data.getParcelableExtra("clientBT");
                    //Log.d("HERE", "ClientBT status: " + clientBluetoothService.getStatus());
                    //menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_bluetooth_disabled_white_24dp));
                    //menu.getItem(0).setTitle("Disconnect device");
                    //Log.d("HERE", "CHANGE ICON");
                    mainListIteams.get(2).setText("Disconnect device");
                    mainListIteams.get(2).setImage(R.drawable.ic_bluetooth_disabled_black_24dp);
                    mainListAdapter = new MainListAdapter(getApplicationContext(), mainListIteams);
                    mainListView.setAdapter(mainListAdapter);
                }
                else {
                    deviceSettings.setConnected(false);
                    //menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_bluetooth_connected_white_24dp));
                    //menu.getItem(0).setTitle("Connect device");
                    //Log.d("HERE", "CHANGE ICON2");
                    mainListIteams.get(2).setText("Connect device");
                    mainListIteams.get(2).setImage(R.drawable.ic_bluetooth_connected_black_24dp);
                    mainListAdapter = new MainListAdapter(getApplicationContext(), mainListIteams);
                    mainListView.setAdapter(mainListAdapter);
                }
                break;
        }
    }
}

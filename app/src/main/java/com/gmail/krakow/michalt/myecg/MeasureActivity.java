package com.gmail.krakow.michalt.myecg;

import android.bluetooth.BluetoothSocket;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import static com.gmail.krakow.michalt.myecg.CommandDecoder.Result._OK;

public class MeasureActivity extends AppCompatActivity {

    private CollectDataClass collectDataClass;

    private TokenValue firstToken;
    private TokenValue secondToken;
    private StringValue msgString1;
    private StringValue msgString2;

    private CommandDecoder commandDecoder;
    private BluetoothSocket mmSocket = null;

    private LineChart mChart;

    @Override
    protected void onStart() {
        super.onStart();
        mmSocket = ((MyECG) this.getApplication()).getSocket();
        collectDataClass = new CollectDataClass();
        collectDataClass.start();
        Log.d("HERE", "Measure: onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        collectDataClass = null;
        mmSocket = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        collectDataClass.stopCollectingData();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure);
        firstToken = new TokenValue();
        secondToken = new TokenValue();
        msgString1 = new StringValue();
        msgString2 = new StringValue();
        setTitle("Measure ECG");
        mChart = (LineChart) findViewById(R.id.chart);

        ArrayList<Entry> yValues = new ArrayList<>();
        yValues.add(new Entry(0,0));
        LineDataSet set1 = new LineDataSet(yValues, "Data Set 1");
        set1.setColor(Color.BLUE);
        set1.setLineWidth(3f);
        set1.setFillAlpha(200);
        set1.setDrawCircles(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        mChart.getDescription().setEnabled(false);
        mChart.getLegend().setEnabled(false);
        mChart.setVisibleXRange(1,151);
        mChart.setData(data);

        Log.d("HERE", "Initialized");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private class CollectDataClass extends Thread {
        private boolean collectData;
        private boolean wait;
        int x;
        int new_y;

        public CollectDataClass() {
            collectData = true;
            wait = false;
            x = 1;
            new_y = 0;


        }

        public void run() {

                    while(collectData) {
                        if(mmSocket == null) break;
                            long time = System.currentTimeMillis();
                            Log.d("HERE","Sending command");
                            //cd = new CommandDecoder();
                            //if(!sendMsg("GET_ECG")) {
                            //    this.cancel();
                            //}
                            BufferedReader in = null;
                            try {
                                time = System.currentTimeMillis();
                                in = new BufferedReader(new InputStreamReader(mmSocket.getInputStream()));
                                PrintWriter out = new PrintWriter(mmSocket.getOutputStream(), true);
                                out.println("GET_ECG");
                                Log.d("SENDING TIME" , String.valueOf(System.currentTimeMillis() - time));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String input = "";
                            try {
                                time = System.currentTimeMillis();
                                input = in.readLine();
                                Log.d("RESPONSE TIME" , String.valueOf(System.currentTimeMillis() - time));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("HERE", "Command sent");

                            //StringValue first_str = new StringValue();
                            //final StringValue second_str = new StringValue();
                            //switch (firstToken.get()) {
                                //case GET_ECG:
                                    //if (secondToken.get() == CommandDecoder.KeywordCode.OK) {
                                        String str_val = "";
                                        if(input.contains("ERR"))
                                            str_val = "0";
                                        else
                                            str_val = input.substring(11);
                                        new_y = Integer.valueOf(str_val);
                                        if(x > 151) {
                                            x = 1;
                                            LineData lineData = mChart.getData();
                                            lineData.removeDataSet(0);

                                            ArrayList<Entry> yValues = new ArrayList<>();
                                            yValues.add(new Entry(0,0));
                                            LineDataSet set1 = new LineDataSet(yValues, "Data Set 1");
                                            set1.setColor(Color.BLUE);
                                            set1.setLineWidth(3f);
                                            set1.setFillAlpha(200);
                                            set1.setDrawCircles(false);

                                            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
                                            dataSets.add(set1);
                                            LineData data = new LineData(dataSets);
                                            mChart.setData(data);


                                        }
                                        LineData lineData = mChart.getData();
                                        lineData.addEntry(new Entry(x++, new_y), 0);
                                        lineData.notifyDataChanged();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                //Log.d("HERE", "Got new value");
                                                mChart.notifyDataSetChanged();
                                                mChart.invalidate();

                                                //if(data != null) {
                                                    //Log.d("HERE", "Data not null");
                                                    //ILineDataSet set = data.getDataSetByIndex(0);
                                                    //Log.d("HERE", "OK");
                                                    //if(set == null) {
                                                        //set = createSet();
                                                        //data.addDataSet(set);
                                                    //}
                                                    //Log.d("HERE", "Set added");

                                                    //data.addEntry(new Entry(set.getEntryCount(), new_y), 0);
                                                    //Log.d("HERE", "Entry added");
                                                    //data.notifyDataChanged();
                                                    //mChart.moveViewToX(data.getEntryCount());
                                                    //Log.d("HERE", "Chart updated");
                                                }


                                            });
                                    }
                                    //break;
                                    //firstToken = new TokenValue();
                                    //secondToken = new TokenValue();
                                    //msgString1 = new StringValue();
                                    //msgString2 = new StringValue();
                            //}
                        //}


        }

        public void stopCollectingData() {
            collectData = false;
        }

        public void setWait(boolean wait) {
            this.wait = wait;
        }
    }

    private boolean sendMsg(String msg) {
        commandDecoder = new CommandDecoder();
        String input = null;
        try {
            BluetoothSocket socket = mmSocket;
            if (socket == null) {

                Log.d("HERE", "NULL socket");
                return false;
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(msg);
            input = in.readLine();
            Log.d("HERE", "Input: " + input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        TokenValue token = new TokenValue();
        TokenValue token2 = new TokenValue();
        StringValue stringToken = new StringValue();
        StringValue stringToken2 = new StringValue();

        for (int i = 0; i < input.length(); i++) {
            commandDecoder.Receiver_PutCharacterToBuffer(input.toCharArray()[i]);
        }
        commandDecoder.Receiver_PutCharacterToBuffer((char) 10);
        if (CommandDecoder.eReceiverStatus.READY == commandDecoder.eReciver_GetStatus()) {
            StringValue received_msg = new StringValue();
            //CommandDecoder.KeywordCode eTokenKey = CommandDecoder.KeywordCode.NULL;
            commandDecoder.Receiver_GetStringCopy(received_msg);
            commandDecoder.DecodeMsg(received_msg);

            if (_OK == commandDecoder.eToken_GetKeywordCode(0, token)) {
                firstToken.set(token.get());

                if (_OK == commandDecoder.eToken_GetKeywordCode(1, token2)) {
                    secondToken.set(token2.get());
                }

                if (_OK == commandDecoder.eToken_GetString(1, stringToken)) {
                    msgString1.set(stringToken.get());
                }

                if (_OK == commandDecoder.eToken_GetString(2, stringToken2)) {
                    msgString2.set(stringToken2.get());
                }
            }
        }
        return true;
    }

}

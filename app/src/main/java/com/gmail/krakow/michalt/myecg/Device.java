package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 27.09.2017.
 */

public class Device {

    private int id;
    private String name;
    private String address;

    public Device(int id, String name, String address) {
        this.name = name;
        this.address = address;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

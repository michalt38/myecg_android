package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 11.02.2018.
 */

public class SignalDate {

    private String id, date;

    public SignalDate(String id, String date) {
        this.id = id;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

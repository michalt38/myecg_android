package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 03.12.2017.
 */

public interface AsyncResponse {
    void getUserData(int id, String name, String surname);
}

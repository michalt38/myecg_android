package com.gmail.krakow.michalt.myecg;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Michał on 01.11.2017.
 */

public class FilesAdapter extends BaseAdapter {

    private Context mContext;
    private List<File> mFilesList;

    public FilesAdapter(Context context, List<File> filesList) {
        this.mContext = context;
        this.mFilesList = filesList;
    }

    @Override
    public int getCount() {
        return mFilesList.size();
    }

    @Override
    public Object getItem(int i) {
        return mFilesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.files_list, null);
        TextView fileName = (TextView)v.findViewById(R.id.file_name);

        fileName.setText(mFilesList.get(i).getName());

        v.setTag(mFilesList.get(i).getId());
        return v;
    }

}

package com.gmail.krakow.michalt.myecg;

/**
 * Created by Michał on 11.09.2017.
 */

public class StringValue {
    private String stringValue;

    public StringValue() {
        stringValue = "";
    }

    public String get() {
        return stringValue;
    }

    public void set(String str) {
        stringValue = str;
    }
}

package com.gmail.krakow.michalt.myecg;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

public class DrawGraphActivity extends AppCompatActivity implements OnChartGestureListener, OnChartValueSelectedListener {

    private static final String TAG = "DrawGraphActivity";
    private LineChart mChart;

    @Override
    protected void onStop() {
        super.onStop();
        mChart.clear();
    }

    ArrayList<Integer> data_val;
    ArrayList<Float> data_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_graph);

        data_val = getIntent().getIntegerArrayListExtra("data_values");
        //data_time = getIntent().getIntegerArrayListExtra("data_times");
        data_time = (ArrayList<Float>) getIntent().getSerializableExtra("data_times");

        mChart = (LineChart)findViewById(R.id.lineChart);

        mChart.setOnChartGestureListener(DrawGraphActivity.this);
        mChart.setOnChartValueSelectedListener(DrawGraphActivity.this);

        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        ArrayList<Entry> yValues = new ArrayList<>();

        Log.d("DATA SIZE", ""+data_val.size());
        if(data_val.size() == 0) {
            Toast.makeText(getApplicationContext(), "No values", Toast.LENGTH_SHORT).show();
            return;
        }
        for(int i = 0; i < data_val.size(); i++) {
            yValues.add(new Entry(data_time.get(i), data_val.get(i)));
        }

        LineDataSet set1 = new LineDataSet(yValues, "Data Set 1");
        set1.setColor(Color.BLUE);
        set1.setLineWidth(3f);
        set1.setFillAlpha(200);
        set1.setDrawCircles(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);
        mChart.getDescription().setEnabled(false);
        mChart.getLegend().setEnabled(false);
        mChart.setMaxVisibleValueCount(150);
        mChart.setData(data);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}

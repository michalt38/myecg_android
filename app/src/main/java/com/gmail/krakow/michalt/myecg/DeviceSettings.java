package com.gmail.krakow.michalt.myecg;

import java.io.Serializable;

/**
 * Created by Michał on 26.09.2017.
 */

public class DeviceSettings implements Serializable{
    private boolean connected;
    private boolean saveSignal;

    public void DeviceSettings() {
        connected = false;
        saveSignal = false;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setSaveSignal(boolean saveSignal) {
        this. saveSignal = saveSignal;
    }

    public boolean getSaveSignal() {
        return saveSignal;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }
}

package com.gmail.krakow.michalt.myecg;
/**
 * Created by Michał on 11.09.2017.
 */

public class TokenValue {
    private CommandDecoder.KeywordCode Key;

    public TokenValue() {
        Key = CommandDecoder.KeywordCode.NULL;
    }

    public CommandDecoder.KeywordCode get() {
        return Key;
    }

    public void set(CommandDecoder.KeywordCode key) {
        Key = key;
    }
}

package com.gmail.krakow.michalt.myecg;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Michał on 27.09.2017.
 */

public class AvailableDevicesAdapter extends BaseAdapter{

    private Context mContext;
    private List<Device> mDeviceList;

    public AvailableDevicesAdapter(Context context, List<Device> deviceList) {
        this.mContext = context;
        this.mDeviceList = deviceList;
    }

    @Override
    public int getCount() {
        return mDeviceList.size();
    }

    @Override
    public Object getItem(int i) {
        return mDeviceList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext, R.layout.available_devices_list, null);
        TextView devName = (TextView)v.findViewById(R.id.device_name);
        TextView devAddress = (TextView)v.findViewById(R.id.device_address);

        devName.setText(mDeviceList.get(i).getName());
        devAddress.setText(mDeviceList.get(i).getAddress());

        v.setTag(mDeviceList.get(i).getId());
        return v;
    }
}
